#include <TimeLib.h>
#include <LiquidCrystal_I2C.h>

#include "./lib/LCD/LCD.h"
#include "./lib/TASK/TASK.h"

void setup() {
  Serial.begin(9600);
  getDateAndTime(__DATE__, __TIME__);
  delay(3000);
  
  // printHeaders();
  configLCD();

}

void loop(){
  task();
}