#ifndef TASK_H
#define TASK_H

unsigned long timeStart = 0;
const unsigned char ACS712 = 0;
const unsigned char ACS175 = 1;
const unsigned char ACS714 = 2;

const unsigned char SENSOR = ACS712;


float totalAmp = 0;

void digitalClockDisplay();
void printDigits(int digits);
void printDigitsLCD(int digits);
bool getDateAndTime(const char *date, const char *time);
void configLCD();
void printLCD();

void task();
void readSensor();
unsigned int readACS(unsigned char pin);
void taskTwo(unsigned long samplingTime);
void taskAnimations(unsigned long samplingTime);
void taskDisplayDateTime(unsigned long samplingTime);

void printWattmeter();
void printHeaders();

void task()
{
  timeStart = millis();
  taskTwo(1000);
  return;
}

void taskTwo(unsigned long samplingTime)
{
  static unsigned long timeTaskTwo = 0;

  if (timeTaskTwo == 0)
    timeTaskTwo = millis();

  if (timeStart >= timeTaskTwo + samplingTime)
  {
    readSensor();
    timeTaskTwo = millis();
  }

  return;
}

void taskAnimations(unsigned long samplingTime)
{
  static unsigned long timeTaskAnimations = 0;
  static unsigned char column = 0;
  static unsigned char cicle = 0;

  if (timeTaskAnimations == 0)
    timeTaskAnimations = millis();

  if (timeStart >= timeTaskAnimations + samplingTime)
  {
    lcd.setCursor(column, 1);

    if (cicle == 0)
    {
      lcd.print("-");
    }
    else if (cicle == 1)
    {
      lcd.print(" ");
    }

    column++;

    if (column == 20)
    {

      column = 0;
      cicle++;

      if (cicle == 2)
      {
        cicle = 0;
      }
    }

    timeTaskAnimations = millis();
  }

  return;
}

void taskDisplayDateTime(unsigned long samplingTime)
{
  static unsigned long timetaskDisplayDateTime = 0;

  if (timetaskDisplayDateTime == 0)
    timetaskDisplayDateTime = millis();

  if (timeStart >= timetaskDisplayDateTime + samplingTime)
  {

    lcd.setCursor(1, 0);
    printDigitsLCD(hour());
    lcd.print(":");
    printDigitsLCD(minute());

    lcd.setCursor(13, 0);
    lcd.print(" ");
    printDigitsLCD(day());
    lcd.print("-");
    printDigitsLCD(month());

    timetaskDisplayDateTime = millis();
  }

  return;
}

void printHeaders() {
  Serial.print("time,");
  Serial.print("vol,");
  Serial.print("mlamp,");
  Serial.print("watts,");
  Serial.print("mlampH,");
  Serial.println("wattsH");
}

unsigned int readACS(unsigned char pin){
    unsigned int readAcs = 0;
    //Validar zonas
    readAcs = analogRead(pin);
    return readAcs;
}

void readSensor() {

    float amp = 0;
    float vol = 0;
    unsigned int res = 0;
    unsigned long adcLong = 0;
    unsigned long adcInt = 0;
    unsigned char counter = 16;

    for (int i = 0; i < counter; i++){
        adcLong += readACS(A0);  
        delayMicroseconds(10); 
    }

    adcInt = (adcLong / counter);
    

    float offset = 1.0;
    float cutInY = 0.0;
    float pending = 0.0;

    if ( SENSOR == 0 ){
        // ACS712
        offset = 12.4;
        cutInY = 511.5f;
        pending = 38.43f;
    }
    else if ( SENSOR == 1 ){
        // ACS715
        offset = 5.0;
        cutInY = 102.3f;
        pending = 38.33f;
    }
    else {
        // ACS714
        offset = 0.0;
        cutInY = 509.5f;
        pending = 20.43f;
    }

    amp = ( ( ( adcInt - ( cutInY - offset ) )  / pending ) ) ;
    unsigned int mlAmpInt = amp * 1000;

    adcInt  = 0;
    adcLong = 0;

    for (int i = 0; i < counter; i++){
        adcLong += readACS(A1);    
    }
    adcInt = (adcLong / counter);

    vol = ( adcInt * 10.53  ) / 722 ;

    const float EFFICIENCY = 0.7;
    float time = (millis() / 1000 );
    time = (time / 3600 );
    float watts    = ( vol * mlAmpInt ) / 1000;
    unsigned int mlampH   = ( time  * mlAmpInt ) / EFFICIENCY;
    float wattsH   = ( vol * mlampH ) / 1000;

    // Serial.print(" time:");

    unsigned long static counterScreen = 0;
    if (counterScreen == 20)
      counterScreen = 0;

    if ( counterScreen % 10 == 0 ){
      lcd.clear();
    }

    if ( counterScreen >= 0 && counterScreen <= 9 ){
      printLCD();
      lcd.setCursor(1, 0);
      lcd.print('0');
      lcd.print(vol,1);
      lcd.setCursor(1, 1);
      printDigitsLCD(mlAmpInt);
      lcd.print(mlAmpInt);
      lcd.setCursor(9, 0);
      lcd.print(watts,3);
      lcd.setCursor(9, 1);
      printDigitsLCD(mlampH);
      lcd.print(mlampH);
    }
    else if ( counterScreen >= 10 && counterScreen <= 19 ){
      unsigned long currentTime = millis();
      unsigned char seconds = (currentTime / 1000) % 60;
      currentTime /= 60;
      unsigned char minutes = (currentTime / 1000) % 60;
      currentTime /= 60;
      unsigned char hours   = (currentTime / 1000) % 24;
      //timmers
      lcd.setCursor(4, 0);
      printDigitsTimeLCD(hours);
      lcd.print(hours);
      lcd.print(":");
      printDigitsTimeLCD(minutes);
      lcd.print(minutes);
      lcd.print(":");
      printDigitsTimeLCD(seconds);
      lcd.print(seconds);

      //time-zone
      lcd.setCursor(4, 1);
      printDigitsTimeLCD(hour());
      lcd.print(hour());
      lcd.print(":");
      printDigitsTimeLCD(minute());
      lcd.print(minute());
      lcd.print(":");
      printDigitsTimeLCD(second());
      lcd.print(second());
    }
    
    counterScreen++;

    // Serial.print(time,5);
    // Serial.print(",");
    // Serial.print(vol);
    // Serial.print(",");
    // Serial.print(mlAmpInt);
    // Serial.print(",");
    // Serial.print(watts, 3);
    // Serial.print(",");
    // Serial.print(mlampH);
    // Serial.print(",");
    // Serial.println(wattsH, 3);


    return;
}

#endif
