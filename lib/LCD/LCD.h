#ifndef LCD_H
#define LCD_H

#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif


// ICONS
const unsigned char ICON_BATERRY          = 1;
const unsigned char ICON_HIGHVOLTAGE      = 2;
const unsigned char ICON_MILIAMPERIOS     = 3;
const unsigned char ICON_VOLTIOS          = 4;
const unsigned char ICON_HOURS            = 5;
const unsigned char ICON_WATTS            = 6;

// Custom character
byte battery[8]       = {0x00,0x04,0x0E,0x0E,0x0E,0x0E,0x0E,0x00}; 
byte highVoltage[8]   = {0x00,0x02,0x04,0x0E,0x0E,0x04,0x08,0x00};
byte miliAmperios[8]  = {0x0A,0x0E,0x0A,0x0A,0x04,0x0A,0x0E,0x0A};
byte voltios[8]       = {0x00,0x00,0x0A,0x0A,0x0A,0x04,0x00,0x00};
byte hours[8]         = {0x00,0x0E,0x04,0x0E,0x0A,0x0E,0x04,0x00};
byte watts[8]         = {0x00,0x00,0x0A,0x0A,0x0E,0x0A,0x00,0x00};

LiquidCrystal_I2C lcd(0x27,16,2);

void digitalClockDisplay();
void printDigits(int digits);
void printDigitsTimeLCD(int digits);
void printDigitsLCD(int digits);
bool getDateAndTime(const char *date, const char *time);
void configLCD();
void printLCD();

void digitalClockDisplay(){
  // digital clock display of the time
  printDigits(day());
  Serial.print("/");
  printDigits(month());
  Serial.print("/");
  printDigits(year()); 
  Serial.print(" "); 
  printDigits(hour());
  Serial.print(":");
  printDigits(minute());
  Serial.print(":");
  printDigits(second());
  Serial.print("");
}

void printDigits(int digits){
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void printDigitsLCD(int digits){
  
  if(digits >= 0 && digits <= 9){
    lcd.print("0000");
  }
  else if (digits >= 10 && digits <= 99){
    lcd.print("000");
  }
  else if (digits >= 100 && digits <= 999){
    lcd.print("00");
  }
  else if (digits >= 1000 && digits <= 9999){
    lcd.print("0");
  }

}

void printDigitsTimeLCD(int digits){
  if(digits < 10)
    lcd.print("0");
}


bool getDateAndTime(const char *date, const char *time)
{
  char Month[12];
  int Day, Year;
  uint8_t monthIndex;

  const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };

  if (sscanf(date, "%s %d %d", Month, &Day, &Year) != 3) return false;
  for (monthIndex = 0; monthIndex < 12; monthIndex++) {
    if (strcmp(Month, monthName[monthIndex]) == 0) break;
  }
  if (monthIndex >= 12) return false;


  int Hour, Min, Sec;

  if (sscanf(time, "%d:%d:%d", &Hour, &Min, &Sec) != 3) return false;

  Hour = Hour + (0);

  setTime(Hour, Min, Sec, Day, monthIndex + 1, Year - 2000);

  // digitalClockDisplay();

  return true;
}

void configLCD(){
  lcd.init();
  lcd.backlight();
  // LCD create
  lcd.createChar(ICON_BATERRY        , battery);
  lcd.createChar(ICON_HIGHVOLTAGE    , highVoltage);
  lcd.createChar(ICON_MILIAMPERIOS   , miliAmperios);
  lcd.createChar(ICON_VOLTIOS        , voltios);
  lcd.createChar(ICON_HOURS          , hours);
  lcd.createChar(ICON_WATTS          , watts);
  lcd.home();
	return;
}


void printLCD(){
  lcd.setCursor(0, 0);
  lcd.write(byte(ICON_BATERRY));
  // lcd.setCursor(1, 0);
  // lcd.print("012.3");
  lcd.setCursor(6, 0);
  lcd.write(byte(ICON_VOLTIOS));

  lcd.setCursor(8, 0);
  lcd.write(byte(ICON_BATERRY));
  // lcd.setCursor(9, 0);
  // lcd.print("00015");
  lcd.setCursor(14, 0);
  lcd.write(byte(ICON_WATTS));
  lcd.setCursor(15, 0);
  lcd.write(byte(ICON_HOURS));

  lcd.setCursor(0, 1);
  lcd.write(byte(ICON_HIGHVOLTAGE));
  // lcd.setCursor(1, 1);
  // lcd.print("00123");
  lcd.setCursor(6, 1);
  lcd.write(byte(ICON_MILIAMPERIOS));

  lcd.setCursor(8, 1);
  lcd.write(byte(ICON_HIGHVOLTAGE));
  // lcd.setCursor(9, 1);
  // lcd.print("00112");
  lcd.setCursor(14, 1);
  lcd.write(byte(ICON_MILIAMPERIOS));
  lcd.setCursor(15, 1);
  lcd.write(byte(ICON_HOURS));
  return;
}


#endif
