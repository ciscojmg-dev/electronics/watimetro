⚡ Wattimetro
=============
### 🛠 Componentes necesarios
  - Arduino
  - ```(A0)``` Sensor de corriente
    - ```ACS712```
    - ```ACS714```
    - ```ACS715```
  - ```(A1)``` Divisor resistivo
  - (Pendiente) LCD

### 👋 Antes de comenzar el programa
  + Debes de setear el sensor de corriente en la variable
    -  ```SENSOR```
      - ```TASK -> TASK.h -> line 9```

### 🔌 Conexiones
  - Grafico

### 💻 Debug
  ```csv
  data.csv

  date/time,vol,mlamp,total-amp,avg-amp,hour-amp,watts,watts-sec,watts-hour
  06/07/2022 15:24:34,11.36,0.128,0.13,0.032,0.000035,1.45,0.001610,34.77
  06/07/2022 15:24:35,11.36,0.128,0.26,0.051,0.000071,1.45,0.002012,34.77
  06/07/2022 15:24:36,11.36,0.101,0.36,0.059,0.000099,1.15,0.001922,27.67
  06/07/2022 15:24:37,11.36,0.128,0.48,0.069,0.000134,1.45,0.002817,34.77
  06/07/2022 15:24:38,11.36,0.101,0.59,0.073,0.000163,1.15,0.002562,27.67
  ```

### 🌐 Referencias
  - https://soloelectronicos.com/2018/06/06/watimetro-con-arduino/

      

